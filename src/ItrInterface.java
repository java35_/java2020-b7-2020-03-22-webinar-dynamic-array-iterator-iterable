
public interface ItrInterface {
	boolean hasNext();
	int next();
	void remove();
}
