import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Appl {
	public static void main(String[] args) {
		int[] ar = new int[10];
		
		DynamicArray array = new DynamicArray();
		ItrIterable itrIterable = new DynamicArray();
		
		
		array.add(10);
		array.add(15);
		array.add(25);
		array.add(35);
		array.add(20);
		array.add(30);
		array.add(40);
		
//		for (int i = 0; i < array.size(); i++) {
//			System.out.println(array.getValueByIndex(i));
//		}
		
//		for (int i = 0; i < array.size(); i++) {
//			if (array.getValueByIndex(i) % 10 == 5) {
//				array.removeByIndex(i);
//				i--;
//			}
//		}
		
//		DynamicArray.Itr itr = array.iterator();
//		while (itr.hasNext()) {
//			int value = itr.next();
//			if (value % 10 == 5)
//				itr.remove();
//		}
		
//		ItrInterface itr = array.iterator();
//			while (itr.hasNext()) {
//			int value = itr.next();
//			if (value % 10 == 5)
//				itr.remove();
//		}
		
		System.out.println("--------------------------");
		for (int i = 0; i < array.size(); i++) {
			System.out.println(array.getValueByIndex(i));
		}
		
		LinkedList ll = new LinkedList();
		
		ll.add();
		ll.add();
		ll.add();
		ll.add();
		
		ItrInterface itr = ll.iterator();
		while (itr.hasNext()) {
			int value = itr.next();
			if (value % 10 == 5)
				itr.remove();
		}
		
		ArrayList<Integer> arrayList = new ArrayList<>();
		Iterator<Integer> itr1 = arrayList.iterator();
		while (itr1.hasNext()) {
			int value = itr1.next();
			if (value % 10 == 5)
				itr1.remove();
		}
		
	}
}
