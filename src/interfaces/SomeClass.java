package interfaces;

public class SomeClass implements SomeInterface {
	int a = 10;
	void classMethod() {
		System.out.println("ClassMethod");
	}
	
	@Override
	public void interfaceMethod() {
		System.out.println("Interface method");
	}
}
