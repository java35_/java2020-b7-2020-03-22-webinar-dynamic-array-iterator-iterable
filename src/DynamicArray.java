public class DynamicArray implements ItrIterable {
	private int[] array = new int[10];
	private int size = 0;
	
	// CRUD
	// C - Create
	// R - Read
	// U - Update
	// D - Delete
	
	// Create
	public void add(int newValue) {
		array[size] = newValue;
		size++;
	}
	
	// Read
	public int getValueByIndex(int index) {
		if (index < 0 || index > size - 1)
			return Integer.MIN_VALUE;
		return array[index];
	}
	
	// Update
	public boolean putValueByIndex(int index, int value) {
		return false;
	}
	
	// Delete
	public void removeByIndex(int index) {
		if (index < 0 || index > size - 1)
			return;
		if (index == size - 1) {
			size--;
			return;
		}
		
		for (int i = index + 1; i < size; i++) {
			array[i - 1] = array[i];
		}
		size--;
	}
	
	public int size() {
		return size;
	}
	
	public class Itr implements ItrInterface {
		int cursor = 0;
		int prevIndex = -1;
		
		@Override
		public boolean hasNext() {
			return cursor != size;
		}
		
		@Override
		public int next() {
//			prevIndex = ++cursor;
			// cursor = cursor + 1;
			// prevIndex = cursor;
			
			// prevIndex = cursor;
			// cursor = cursor + 1
			
//			prevIndex = cursor;
//			cursor = cursor + 1; // cursor++ : ++cursor
			
			prevIndex = cursor++;
			return array[prevIndex];
		}
		
		@Override
		public void remove() {
			removeByIndex(prevIndex);
			cursor--;
		}
	}

	@Override
	public ItrInterface iterator() {
		return new Itr();
	}
}